package de.friedenhagen.releasetest

import io.kotlintest.specs.ShouldSpec

class AppKotlinShouldTest : ShouldSpec() {
    init {
        val adultSUT = AppKotlin(18)
        "An adult user" {
            should("have age 18") {
                adultSUT.age shouldEqual 18
            }
            should("be adult") {
                adultSUT.isOldEnough() shouldBe true
            }
        }
        val minorSUT = AppKotlin(10)
        "A minor user" {
            should("have age 10") {
                minorSUT.age shouldEqual 10
            }
            should("not be adult") {
                minorSUT.isOldEnough() shouldBe false
            }
        }
    }
}