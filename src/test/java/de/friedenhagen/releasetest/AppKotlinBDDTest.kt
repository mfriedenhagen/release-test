package de.friedenhagen.releasetest

import io.kotlintest.specs.BehaviorSpec

class AppKotlinBDDTest : BehaviorSpec() {
    init {
        Given("a user with age 18") {
            val sut = AppKotlin(18)
            When("asking for his age") {
                val result = sut.age
                Then("it should be 18") {
                    result shouldEqual 18
                }
            }
            When("checking wether he is old enough") {
                Then("it should be true") { assert(sut.isOldEnough()) }
            }
        }
    }
}